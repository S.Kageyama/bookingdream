import { AdminLayout, HeaderOnly } from '~/components/Layout';

import Home from '~/pages/Home';
import Following from '~/pages/Following';
import Profile from '~/pages/Profile';
import Upload from '~/pages/Upload';
import Search from '~/pages/Search';
import Dashboard from '~/pages/Admin/Dashboard';
import { addCustomer, listCustomer } from '~/pages/Admin/Customer';
import { listRoom } from '~/pages/Admin/Room';
import { listService } from '~/pages/Admin/Services';

//public Routes
const publicRoutes = [
    {
        path: '/',
        component: Home,
    },
    {
        path: '/following',
        component: Following,
    },
    {
        path: '/profile',
        component: Profile,
    },
    {
        path: '/upload',
        component: Upload,
        layout: HeaderOnly,
    },
    {
        path: '/search',
        component: Search,
        layout: null,
    },
    {
        path: '/admin',
        component: Dashboard,
        layout: AdminLayout,
    },
    {
        path: '/admin/customer',
        component: listCustomer,
        layout: AdminLayout,
    },
    {
        path: '/admin/customer/add',
        component: addCustomer,
        layout: AdminLayout,
    },
    {
        path: '/admin/room',
        component: listRoom,
        layout: AdminLayout,
    },
    {
        path: '/admin/service',
        component: listService,
        layout: AdminLayout,
    },
];

const privateRoutes = [];

export { publicRoutes, privateRoutes };
